# Curso Angular: De cero a experto (Legacy)

## Proyecto SPA-Heroes

Instruido por:

Fernando Herrera

[Agular-Basico-Legacy](https://www.udemy.com/course/angular-2-fernando-herrera/)

Se recomienda ver el curso Actualizado [Angular-Basico-2021](https://www.udemy.com/course/angular-fernando-herrera/)

![Portada](https://i.imgur.com/ydoWLSw.png)

### Comandos:

- `npm install bootstrap`

colocarlo en styles.css : `@import "~bootstrap/dist/css/bootstrap.css"`, para usar los estilos de bootstrap, existen otras formas de usar bootstrap como por CDN.

Para evitar errores de iniciazlizacion, colocar en tsconfig.json:

- `"strictPropertyInitialization": false,`
